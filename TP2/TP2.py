class box(object):
    
    def __init__(self):
        self._contents = []
        self._ouverture=True
        self.cap=None
        
    def add(self,truc):
        self._contents.append(truc)
        
    def __contains__(self,machin):
        return (machin in self._contents)
        
    def suppr(self,truc):
        self._contents.remove(truc)
        
    def close(self):
        self._ouverture=False
        
    def open(self):
        self._ouverture=True
        
    def is_open(self):
        return self._ouverture
    
    def action_look(self):
        if len(self._contents)==0:
            res = "la boite est fermee"
        else:
            res = "la boite contient: "
            res+=", ".join(self._contents)
        return res
    
    def set_capacity(self, cap):
        self.cap=cap
        
    def capacity(self):
        return self.cap
    
    def has_room_for(self,t):
        res=True
        place_libre=self.capacity()-t.volume()
        for th in self._contents:
            place_libre-=th.volume()
        if place_libre <0:
            res=False
        return res
    
    
class Thing(object):
    
    def __init__(self,volume):
        self.vol=volume
        
    def volume(self):
        return self.vol
    

    
    
    
    
        

